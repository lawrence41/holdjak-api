#![allow(dead_code)]

use std::fmt::Debug;
use std::ops::Deref;

use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::entry::Entry::{ChallengeAttempt, RegisterChallenge, Send};
use crate::entry::{
    ChallengeAnswer, ChallengeAttemptEntry, ChallengeEntry, Entry, PlayerEntry, SendEntry,
    SendRequest,
};

#[derive(Clone)]
pub struct HoldjakInMemory {
    id: Uuid,
    ledger: Vec<Entry>,
}

impl HoldjakInMemory {
    fn push_to_ledger<T: Into<Entry>>(&mut self, entry: T) {
        self.ledger.push(entry.into());
    }
    pub fn new() -> Self {
        HoldjakInMemory::default()
    }

    fn get_challenge_status(&self, id: &String, player_id: &String) -> bool {
        self.ledger
            .iter()
            .find_map(|entry: &Entry| match entry {
                ChallengeAttempt(ChallengeAttemptEntry {
                    to, challenge_id, ..
                }) => {
                    if player_id.eq(to) && id.eq(challenge_id) {
                        Some(true)
                    } else {
                        None
                    }
                }
                _ => None,
            })
            .is_none()
    }

    pub fn get_applicable_challenges(
        &self,
        player_id: String,
    ) -> impl Iterator<Item = &ChallengeEntry> {
        self.ledger.iter().filter_map(move |entry| match entry {
            RegisterChallenge(challenge) => {
                if player_id.eq(&challenge.to)
                    && self.get_challenge_status(&challenge.id, &player_id)
                {
                    return Some(challenge);
                }
                None
            }
            _ => None,
        })
    }

    pub fn get_challenges(&self) -> impl Iterator<Item = &ChallengeEntry> {
        self.ledger.iter().filter_map(|entry| match entry {
            RegisterChallenge(challenge) => Some(challenge),
            _ => None,
        })
    }
    pub fn get_players(&self) -> impl Iterator<Item = &PlayerEntry> {
        self.ledger.iter().filter_map(|entry| match entry {
            Entry::Player(player) => Some(player),
            _ => None,
        })
    }
    pub fn get_player_by_id(&self, id: &str) -> Option<PlayerEntry> {
        self.get_players().find(|player| player.id.eq(id)).cloned()
    }
    pub fn register_player(&mut self, player: &PlayerRequest) -> PlayerEntry {
        let add_player_entry = PlayerEntry::new(player.name.as_str(), player.id.as_str());
        self.push_to_ledger(add_player_entry.clone());
        self.add_marbles(2000, &add_player_entry.id);
        add_player_entry
    }
    pub fn get_ledger(&self) -> Vec<Entry> {
        self.ledger.clone()
    }
    pub fn add_marbles(&mut self, amount: i64, to: &str) -> SendEntry {
        let entry = SendEntry::new(&self.id.to_string(), to, amount);
        self.push_to_ledger(entry.clone());
        entry
    }
    pub fn get_balance_by_id(&self, player_id: &str) -> i64 {
        self.get_ledger()
            .into_iter()
            .map(|entry| match entry {
                Send(SendEntry {
                    from, to, amount, ..
                }) => Self::calculate_send(from, to, amount, player_id),
                ChallengeAttempt(ChallengeAttemptEntry {
                    correct,
                    amount,
                    to,
                    from,
                    ..
                }) => Self::calculate_balance_of_challenge_attempts(
                    correct, amount, to, from, player_id,
                ),
                _ => 0,
            })
            .reduce(|a, b| a + b)
            .unwrap_or(0)
    }
    fn calculate_send(from: String, to: String, amount: i64, player_id: &str) -> i64 {
        if from.eq(player_id) {
            -amount
        } else if to.eq(player_id) {
            amount
        } else {
            0
        }
    }
    fn calculate_balance_of_challenge_attempts(
        correct: bool,
        amount: i64,
        to: String,
        from: String,
        player_id: &str,
    ) -> i64 {
        if to.eq(&player_id) {
            if correct {
                amount
            } else {
                -amount
            }
        } else if from.eq(&player_id) {
            if !correct {
                amount
            } else {
                -amount
            }
        } else {
            0
        }
    }
    pub fn send(&mut self, from: &str, to: &str, amount: i64) -> Result<SendEntry, ()> {
        let send_entry = SendEntry::new(from, to, amount);
        let balance = self.get_balance_by_id(from);
        if balance >= amount {
            self.push_to_ledger(send_entry.clone());
            Ok(send_entry)
        } else {
            Err(())
        }
    }

    pub fn get_challenge_by_id(&self, qid: &str) -> Option<ChallengeEntry> {
        self.ledger
            .clone()
            .into_iter()
            .find_map(|entry| match entry {
                RegisterChallenge(entry) => {
                    if entry.id.eq(qid) {
                        return Some(entry);
                    }
                    None
                }
                _ => None,
            })
    }
    pub fn challenge(
        &mut self,
        from: &String,
        to: &String,
        amount: i64,
    ) -> Result<ChallengeEntry, ChallengeError> {
        let balance = self.get_balance_by_id(from);
        if balance == 0 {
            return Err(ChallengeError("No Marbles :(".to_string()));
        } else if balance < amount {
            return Err(ChallengeError("Not Enough Marbles!".to_string()));
        } else if amount < 1 {
            return Err(ChallengeError("Why even try?".to_string()));
        }
        let register_challenge_entry = ChallengeEntry::new(from, to, amount);
        self.push_to_ledger(register_challenge_entry.clone());
        Ok(register_challenge_entry)
    }
    pub fn attempt_challenge(
        &mut self,
        challenge_id: &str,
        challenge_answer: &ChallengeAnswer,
        player_id: &String,
    ) -> Result<ChallengeAttemptEntry, ChallengeAttemptError> {
        let balance = self.get_balance_by_id(player_id);
        if balance == 0 {
            return Err(ChallengeAttemptError("No Marbles :("));
        }

        let maybe_entry = self.get_challenge_by_id(challenge_id).map(|entry| {
            let ChallengeEntry {
                amount, to, from, ..
            } = entry;

            if !to.eq(player_id) {
                None
            } else {
                Some((amount, from))
            }
        });
        if let Some((amount, from)) = maybe_entry.unwrap() {
            let correct = match challenge_answer {
                ChallengeAnswer::Odd => amount % 2 == 1,
                ChallengeAnswer::Even => amount % 2 == 0,
            };

            let answer_challenge_entry = ChallengeAttemptEntry::new(
                challenge_id,
                challenge_answer.clone(),
                amount,
                correct,
                player_id,
                &from,
            );
            self.push_to_ledger(answer_challenge_entry.clone());
            Ok(answer_challenge_entry)
        } else {
            Err(ChallengeAttemptError("Can't Attempt!"))
        }
    }
}

impl Default for HoldjakInMemory {
    fn default() -> Self {
        HoldjakInMemory {
            id: Uuid::new_v4(),
            ledger: vec![],
        }
    }
}
#[derive(Serialize, Debug)]
pub struct ChallengeError(pub String);

impl Deref for ChallengeError {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChallengeAttemptRequest {
    pub answer: ChallengeAnswer,
}

#[derive(Deserialize)]
pub struct ChallengeAttemptError<'a>(&'a str);

#[cfg(test)]
mod tests {
    use super::*;
    use crate::entry::ChallengeAnswer::Odd;

    #[test]
    pub fn new_player() {
        let mut game = HoldjakInMemory::new();
        assert_eq!(game.ledger.len(), 0);
        let player_id = game.register_player(&PlayerRequest {
            name: "sl;kdafj".to_string(),
            id: "asdjkl;fh".to_string(),
        });
        assert_eq!(game.ledger.len(), 2);
        println!("{:?}", player_id)
    }

    #[test]
    pub fn init_balance() {
        let mut game = HoldjakInMemory::new();
        let id = game
            .register_player(&PlayerRequest {
                name: "sl;kdafj".to_string(),
                id: "asdjkl;fh".to_string(),
            })
            .id;
        let balance = game.get_balance_by_id(&id);
        assert_eq!(balance, 20);
        let res = game.add_marbles(10, &id);
        let balance = game.get_balance_by_id(&id);
        assert_eq!(balance, 30);
    }

    #[test]
    pub fn transfer() {
        let mut game = HoldjakInMemory::new();
        let player_1 = game
            .register_player(&PlayerRequest {
                name: "sl;kdafj".to_string(),
                id: "asdjkl;fh".to_string(),
            })
            .id;
        let player_2 = game
            .register_player(&PlayerRequest {
                name: "sl;kdafj".to_string(),
                id: "asdjkl;fh".to_string(),
            })
            .id;
        game.send(&player_1, &player_2, 5);
        let player_1_balance = game.get_balance_by_id(&player_1);
        let player_2_balance = game.get_balance_by_id(&player_2);

        assert_eq!(player_1_balance, 15);
        assert_eq!(player_2_balance, 25);

        let result = game.send(&player_1, &player_2, 16);
        assert_eq!(result.is_ok(), false);
    }

    #[test]
    pub fn challenge() {
        let mut game = HoldjakInMemory::new();
        let player_1 = game
            .register_player(&PlayerRequest {
                name: "sl;kdafj".to_string(),
                id: "asdjkl;fh".to_string(),
            })
            .id;
        let player_2 = game
            .register_player(&PlayerRequest {
                name: "sl;kdafj".to_string(),
                id: "asdjkl;fh".to_string(),
            })
            .id;

        let send_entry: ChallengeEntry = game.challenge(&player_1, &player_2, 5).unwrap();

        let player_1_balance = game.get_balance_by_id(&player_1);
        let player_2_balance = game.get_balance_by_id(&player_2);

        assert_eq!(player_1_balance, 20);
        assert_eq!(player_2_balance, 20);

        game.attempt_challenge(&send_entry.id, &Odd, &player_2).ok();
        let player_1_balance = game.get_balance_by_id(&player_1);
        let player_2_balance = game.get_balance_by_id(&player_2);
        assert_eq!(player_1_balance, 15);
        assert_eq!(player_2_balance, 25);
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct PlayerRequest {
    pub name: String,
    pub id: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChallengeRequest {
    pub to: String,
    pub amount: i64,
}
