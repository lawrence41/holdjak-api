use crate::{FutureMutex, HoldjackPlayer, HoldjakInMemory, ServerState};
use firebase_admin::firestore::Firestore;
use firebase_admin::user::FirebaseUser;
use rocket::http::Status;
use rocket::outcome::Outcome::{Failure, Success};
use rocket::request::{FromRequest, Outcome};
use rocket::{Request, State};

pub struct HoldjakFirestoreClient<'r> {
    firestore_client_mutex: &'r State<FutureMutex<Firestore>>,
    holdjak: &'r FutureMutex<HoldjakInMemory>,
}
pub struct HoldjakFirestore<'r>(HoldjakFirestoreClient<'r>);

#[async_trait]
impl<'r> FromRequest<'r> for HoldjakFirestore<'r> {
    type Error = ();
    async fn from_request(request: &'r Request<'_>) -> Outcome<HoldjakFirestore<'r>, ()> {
        let state = request.guard::<&State<ServerState>>().await.unwrap();
        let user = request.guard::<FirebaseUser>().await.unwrap();

        if !user.authed || user.user_id.is_empty() {
            return Failure((Status::Unauthorized, ()));
        }

        let mut firestore_client_mutex = request
            .guard::<&State<FutureMutex<Firestore>>>()
            .await
            .unwrap();

        let mut holdjak = &state.game;
        Success(HoldjakFirestore(HoldjakFirestoreClient {
            firestore_client_mutex,
            holdjak,
        }))
    }
}
