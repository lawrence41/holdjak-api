use std::path::PathBuf;

use rocket::{
    fairing::{Fairing, Info, Kind},
    http::Header,
    Build, Request, Response, Rocket,
};

pub struct Cors;

const OPTIONS_STRING: &'_ str = "POST, GET, PATCH, OPTIONS";

#[options("/<_path..>")]
pub fn get_options(_path: PathBuf) -> String {
    OPTIONS_STRING.to_string()
}

#[rocket::async_trait]
impl Fairing for Cors {
    fn info(&self) -> Info {
        Info {
            name: "Holdjak API",
            kind: Kind::Response | Kind::Ignite,
        }
    }

    async fn on_ignite(&self, rocket: Rocket<Build>) -> rocket::fairing::Result {
        Ok(rocket.mount("/", routes![get_options]))
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new(
            "Access-Control-Allow-Origin",
            "http://localhost:4200",
        ));
        response.set_header(Header::new("Access-Control-Allow-Methods", OPTIONS_STRING));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
        response.set_header(Header::new("Access-Control-Expose-Headers", "*"));
    }
}
