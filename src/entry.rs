#[allow(unused_imports)]
use chrono::Utc;
use firebase_admin::value::FirestoreValue;
use firebase_admin::*;
use lerp::Lerp;
use rand::prelude::*;
use rocket::form::error::Entity::Value;
use sandy::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::process::id;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, FromEnumOfTuples)]
#[serde(tag = "@type")]
pub enum Entry {
    Send(SendEntry),
    Player(PlayerEntry),
    RegisterChallenge(ChallengeEntry),
    ChallengeAttempt(ChallengeAttemptEntry),
}

pub fn generate_id() -> (String, String) {
    (Uuid::new_v4().to_string(), Utc::now().to_string())
}

const BIG_RANDY: i64 = 11572;
macro_rules! hash_for_entry {
    ($($arg:expr),*) => {{
        let mut hasher = Sha256::new();
        $( hasher.update(&$arg.to_string().as_bytes()); )*
        format!("{:X}", hasher.finalize())
    }}
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct SendRequest {
    pub amount: i64,
}

#[derive(Debug, Clone, Serialize, FromFirestoreValue)]
pub struct SendEntry {
    pub ts: String,
    pub id: String,
    pub from: String,
    pub to: String,
    #[serde(skip_serializing)]
    pub amount: i64,
}

impl SendEntry {
    pub fn new(from: &str, to: &str, amount: i64) -> Self {
        let (id, ts) = generate_id();
        Self {
            id,
            ts,
            from: from.to_string(),
            to: to.to_string(),
            amount,
        }
    }
}

#[derive(Debug, Clone, Serialize, FromFirestoreValue)]
pub struct PlayerEntry {
    pub ts: String,
    pub id: String,
    pub name: String,
}

impl PlayerEntry {
    pub fn new(name: &str, id: &str) -> Self {
        let (rand_id, ts) = generate_id();
        PlayerEntry {
            ts,
            name: String::from(name),
            id: String::from(id),
        }
    }
}

impl Default for PlayerEntry {
    fn default() -> Self {
        let (id, ts) = generate_id();
        PlayerEntry {
            ts,
            id,
            name: String::from(""),
        }
    }
}

#[derive(Debug, Clone, Serialize, Default, FromFirestoreValue)]
pub struct ChallengeEntry {
    pub ts: String,
    pub id: String,

    pub from: String,
    pub to: String,
    pub size: i64,
    #[serde(skip_serializing)]
    pub amount: i64,
}

impl ChallengeEntry {
    pub fn new(from: &String, to: &String, amount: i64) -> Self {
        let mut rng = rand::thread_rng();
        let y: f32 = rng.gen();
        let (id, ts) = generate_id();
        Self {
            id,
            ts,
            from: from.clone(),
            to: to.clone(),
            size: (0_0_f32.lerp(10_f32, amount as f32) * y) as i64,
            amount,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, FromFirestoreValue)]
pub struct ChallengeAttemptEntry {
    pub id: String,
    pub ts: String,
    pub challenge_id: String,
    pub answer: ChallengeAnswer,
    pub amount: i64,
    pub correct: bool,
    pub to: String,
    pub from: String,
}

impl ChallengeAttemptEntry {
    pub fn new(
        challenge_id: &str,
        answer: ChallengeAnswer,
        amount: i64,
        correct: bool,
        to: &String,
        from: &String,
    ) -> Self {
        let (id, ts) = generate_id();
        Self {
            id,
            ts,
            answer,
            to: to.clone(),
            from: from.clone(),
            amount,
            challenge_id: String::from(challenge_id),
            correct,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ChallengeAnswer {
    Odd,
    Even,
}
impl From<ChallengeAnswer> for FirestoreValue {
    fn from(challenge_answer: ChallengeAnswer) -> Self {
        match challenge_answer {
            ChallengeAnswer::Odd => FirestoreValue::from("Odd"),
            ChallengeAnswer::Even => FirestoreValue::from("Even"),
        }
    }
}

impl From<FirestoreValue> for ChallengeAnswer {
    fn from(firestore_value: FirestoreValue) -> Self {
        let answer: String = firestore_value.try_into().unwrap();
        if answer == "Odd".to_string() {
            return Self::Odd;
        }
        if answer == "Even".to_string() {
            return Self::Even;
        }
        panic!("Has to be odd or even!");
    }
}

impl ToString for ChallengeAnswer {
    fn to_string(&self) -> String {
        match self {
            Self::Odd => "Odd".to_string(),
            Self::Even => "Even".to_string(),
        }
    }
}

macro_rules! get_field {
    ($fields: ident, $field: ident) => {{
        let value: FirestoreValue = $fields.get(stringify!($field)).unwrap().into();
        value.try_into().unwrap()
    }};
}

// impl From<FirestoreValue> for PlayerEntry {
//     fn from(value: FirestoreValue) -> Self {
//         match value.0.value_type.unwrap() {
//             firebase_admin::ValueType::MapValue(firebase_admin::MapValue { fields }) => {
//                 let ts = get_field!(fields, ts);
//                 let id = get_field!(fields, id);
//                 let name = get_field!(fields, name);
//                 Self { ts, id, name }
//             }
//             _ => panic!("Shit!"),
//         }
//     }
// }

// impl From<FirestoreValue> for ChallengeEntry {
//     fn from(value: FirestoreValue) -> Self {
//         match value.0.value_type.unwrap() {
//             firebase_admin::ValueType::MapValue(firebase_admin::MapValue { fields }) => {
//                 let ts = get_field!(fields, ts);
//                 let id = get_field!(fields, id);
//                 let from = get_field!(fields, from);
//                 let to = get_field!(fields, to);
//                 let size = get_field!(fields, size);
//                 let amount = get_field!(fields, amount);
//                 ChallengeEntry {
//                     ts,
//                     id,
//                     from,
//                     to,
//                     size,
//                     amount,
//                 }
//             }
//             _ => panic!("Shit!"),
//         }
//     }
// }

#[cfg(test)]
mod test {
    use firebase_admin::value::FirestoreValue;
    use firebase_admin::{Value, ValueType};

    use crate::Entry::Player;
    use crate::{ChallengeEntry, Entry, PlayerEntry};
    use firebase_admin::*;

    #[derive(FromFirestoreValue)]
    pub struct Something {
        id: String,
        ts: String,
    }

    fn write_something_with_value(value: Value) {}

    #[test]
    fn test_conversion() {
        let player = PlayerEntry::new("lawrence", "12345");
        let firestorevalue = FirestoreValue::from(player);
        write_something_with_value(firestorevalue.0);
        let something = Something {
            id: "some".to_string(),
            ts: "thing".to_string(),
        };
        let value: FirestoreValue = something.into();
        let something_back: Something = Something::from(value);
        assert_eq!(1, 1);
    }
}
