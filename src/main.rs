extern crate core;
#[macro_use]
extern crate rocket;

use firebase_admin::*;
use futures::lock::Mutex as FutureMutex;
use rocket::http::Status;
use rocket::outcome::Outcome::{Failure, Success};
use rocket::request::{FromRequest, Outcome};
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::{futures, Request, State};
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};

use entry::{ChallengeAttemptEntry, ChallengeEntry, Entry, PlayerEntry};
use firebase_admin::fairing::FirebaseConfigMode::Env;
use firebase_admin::fairing::FirebaseFairing;
use firebase_admin::firestore::Firestore;
use firebase_admin::user::FirebaseUser;
use firebase_admin::value::FirestoreValue;

use crate::cors::Cors;
use crate::entry::{generate_id, SendRequest};
use crate::holdjak::{ChallengeAttemptRequest, ChallengeRequest, HoldjakInMemory, PlayerRequest};

mod cors;
mod entry;
mod firestore;
mod holdjak;

#[get("/entries")]
async fn get_entries(state: &State<ServerState>) -> Json<Vec<Entry>> {
    Json(state.game.lock().await.get_ledger())
}

#[derive(FromFirestoreValue)]
struct Foo {
    bar: String,
}

#[post("/challenges", data = "<challenge_request>")]
async fn register_challenge(
    state: &State<ServerState>,
    challenge_request: Json<ChallengeRequest>,
    player: HoldjackPlayer,
    firestore_mutex: &State<FutureMutex<Firestore>>,
) -> Result<Json<ChallengeEntry>, status::BadRequest<String>> {
    let player = &player.0.unwrap();
    let player_id = &player.id;
    match state.game.lock().await.challenge(
        player_id,
        &challenge_request.to,
        challenge_request.amount,
    ) {
        Ok(challenge_entry) => {
            let mut firestore = firestore_mutex.lock().await;
            let ChallengeEntry {
                id, from, size, to, ..
            } = challenge_entry.clone();
            let player_query = format!("users/{}", challenge_entry.to.as_str());
            let mut player_record = firestore
                .get_firestore_document::<PlayerRecord>(&player_query)
                .await
                .unwrap();
            player_record.challenges.push(challenge_entry.clone());
            firestore
                .upsert_document(&player_query, player_record.into())
                .await;
            Ok(Json(challenge_entry))
        }
        Err(message) => Err(status::BadRequest(Some(message.0))),
    }
}

#[get("/challenges")]
async fn get_challenges(state: &State<ServerState>) -> Json<Vec<ChallengeEntry>> {
    let holdjak = state.game.lock().await;
    Json(holdjak.get_challenges().cloned().collect())
}

type GetChallengeResponse = Result<Json<ChallengeEntry>, status::Custom<&'static str>>;

#[get("/challenges/<challenge_id>")]
async fn get_challenge(
    state: &State<ServerState>,
    challenge_id: String,
    player: HoldjackPlayer,
) -> GetChallengeResponse {
    if player.0.is_some() {
        let holdjak = state.game.lock().await;
        if let Some(challenge_entry) = holdjak.get_challenge_by_id(&challenge_id) {
            return Ok(Json(challenge_entry));
        }
        Err(status::Custom(
            Status::NotFound,
            "no challenge with that id",
        ))
    } else {
        Err(status::Custom(Status::Forbidden, "not allowed"))
    }
}

#[get("/self/challenges")]
async fn get_self_challenges(
    state: &State<ServerState>,
    player: HoldjackPlayer,
) -> Json<Vec<ChallengeEntry>> {
    let holdjak = state.game.lock().await;
    let player_id = player.0.unwrap().id;
    Json(
        holdjak
            .get_applicable_challenges(player_id)
            .cloned()
            .collect(),
    )
}

type AttemptChallengeResponse<'a> = Result<Json<ChallengeAttemptEntry>, status::NotFound<&'a str>>;

#[post("/challenges/<challenge_id>/attempt", data = "<attempt_challenge>")]
async fn attempt_challenge<'a>(
    state: &State<ServerState>,
    attempt_challenge: Json<ChallengeAttemptRequest>,
    challenge_id: String,
    player: HoldjackPlayer,
    firestore_mutex: &State<FutureMutex<Firestore>>,
) -> AttemptChallengeResponse<'a> {
    if let Ok(challenge_entry) = state.game.lock().await.attempt_challenge(
        &challenge_id,
        &attempt_challenge.answer,
        &player.0.unwrap().id,
    ) {
        let player_query = format!("users/{}", &challenge_entry.to);
        let mut firebase = firestore_mutex.lock().await;
        let mut player_record: PlayerRecord = firebase
            .get_firestore_document::<PlayerRecord>(&player_query)
            .await
            .unwrap();
        let challenges: Vec<ChallengeEntry> = player_record
            .challenges
            .into_iter()
            .filter(|c| c.id != challenge_id)
            .collect();
        player_record.challenges = challenges;
        firebase
            .upsert_document(&player_query, player_record.into())
            .await;
        Ok(Json(challenge_entry))
    } else {
        Err(status::NotFound("No Entry Found"))
    }
}

#[get("/self/balance")]
async fn get_self_balance(state: &State<ServerState>, user: FirebaseUser) -> Json<i64> {
    let game = state.game.lock().await;
    let maybe_player = game.get_player_by_id(user.user_id.as_str());
    if let Some(player) = maybe_player {
        Json(game.get_balance_by_id(player.id.as_str()))
    } else {
        Json(0)
    }
}

#[get("/self")]
fn get_self(player: HoldjackPlayer) -> Json<PlayerEntry> {
    Json(player.0.unwrap())
}

type SendRequestResult = Result<status::Custom<Json<SendRequest>>, status::Forbidden<()>>;

#[post("/players/<player_id>/send", data = "<send_request>")]
async fn send_marbles_to_player(
    state: &State<ServerState>,
    player: HoldjackPlayer,
    send_request: Json<SendRequest>,
    player_id: &str,
) -> SendRequestResult {
    match &*player {
        Some(player) => {
            state
                .game
                .lock()
                .await
                .send(&player.id, player_id, send_request.amount)
                .ok();
            Ok(status::Custom(Status::Ok, send_request))
        }
        _ => Err(status::Forbidden(None)),
    }
}

#[post("/players", data = "<register_player_message>")]
async fn register_player(
    state: &State<ServerState>,
    register_player_message: Json<PlayerRequest>,
) -> Json<PlayerEntry> {
    let mut game = state.game.lock().await;
    Json(game.register_player(&register_player_message))
}

#[get("/players")]
async fn get_players(state: &State<ServerState>) -> Json<Vec<PlayerEntry>> {
    Json(state.game.lock().await.get_players().cloned().collect())
}

pub struct ServerState {
    game: FutureMutex<HoldjakInMemory>,
}

impl ServerState {
    fn new() -> Self {
        Self {
            game: FutureMutex::new(HoldjakInMemory::new()),
        }
    }
}

impl Deref for HoldjackPlayer {
    type Target = Option<PlayerEntry>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for HoldjackPlayer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct HoldjackPlayer(Option<PlayerEntry>);

#[derive(FromFirestoreValue, Debug)]
pub struct Marble {
    id: String,
    ts: String,
    color: String,
    from: String,
}
impl Marble {
    pub fn new(from: &str, color: &str) -> Self {
        let (id, ts) = generate_id();
        Self {
            id,
            ts,
            color: color.to_string(),
            from: from.to_string(),
        }
    }
}
#[derive(FromFirestoreValue, Debug)]
pub struct PlayerRecord {
    balance: i64,
    marbles: Vec<Marble>,
    id: String,
    ts: String,
    name: String,
    challenges: Vec<ChallengeEntry>,
}
fn generate_marble_vec(from: &str, amount: i64) -> Vec<Marble> {
    let mut marbles = vec![];
    let color = from
        .as_bytes()
        .into_iter()
        .map(|v| format!("{:#02}", v))
        .collect::<Vec<String>>()
        .join("");

    for n in 1..amount {
        marbles.push(Marble::new(from, color.as_str()));
    }
    marbles
}

#[async_trait]
impl<'r> FromRequest<'r> for HoldjackPlayer {
    type Error = ();
    async fn from_request(request: &'r Request<'_>) -> Outcome<HoldjackPlayer, ()> {
        let state = request.guard::<&State<ServerState>>().await.unwrap();
        let user = request.guard::<FirebaseUser>().await.unwrap();
        let firestore = request
            .guard::<&State<FutureMutex<Firestore>>>()
            .await
            .unwrap();
        let mut firestore = firestore.lock().await;

        if !user.authed || user.user_id.is_empty() {
            return Failure((Status::Unauthorized, ()));
        }
        let mut holdjak = state.game.lock().await;
        let outcome = if let Some(player_entry) = holdjak.get_player_by_id(user.user_id.as_str()) {
            return Success(HoldjackPlayer(Some(player_entry)));
        } else {
            let player_entry = holdjak.register_player(&PlayerRequest {
                name: user.name,
                id: user.user_id,
            });
            let marbles = generate_marble_vec("the-home-base", 2000);
            let player_record = PlayerRecord {
                balance: 2000,
                id: player_entry.id.to_string(),
                name: player_entry.name.to_string(),
                challenges: vec![],
                ts: player_entry.ts.to_string(),
                marbles,
            };
            let res = firestore
                .upsert_document(
                    format!("users/{}", &player_entry.id).as_str(),
                    player_record.into(),
                )
                .await;
            if let Err(e) = res {
                print!("ERROR :: {e:?}");
            }
            return Success(HoldjackPlayer(Some(player_entry)));
        };
    }
}

#[launch]
async fn rocket() -> _ {
    let state = ServerState::new();
    let firebase_fairing = FirebaseFairing::new(Env);

    rocket::build()
        .manage(state)
        .attach(Cors)
        .attach(firebase_fairing)
        .mount(
            "/api/v1/",
            routes![
                get_players,
                send_marbles_to_player,
                get_entries,
                get_self_balance,
                get_self_challenges,
                get_self,
                register_player,
                register_challenge,
                attempt_challenge,
                get_challenges,
                get_challenge,
            ],
        )
}
