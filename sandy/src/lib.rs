#![allow(warnings)]
extern crate core;
extern crate proc_macro;

use proc_macro::TokenStream;

use quote::{quote, ToTokens};
use std::str::FromStr;
use syn::{
    parse::Parse, parse::ParseStream, parse_macro_input, Data, DataEnum, DeriveInput, Fields,
    Ident, Result, Type,
};

macro_rules! impl_try_into_struct_entry {
    ($i: tt, $o: tt) => {
        impl TryFrom<Entry> for $o {
            type Error = ();
            fn try_from(entry: Entry) -> Result<Self, Self::Error> {
                match entry {
                    Entry::$i(item) => Ok(item),
                    _ => Err(()),
                }
            }
        }
        impl From<$o> for Entry {
            fn from(player: $o) -> Self {
                Self::$i(player)
            }
        }
    };
}

#[derive(Debug)]
struct FileName {
    filename: String,
}
impl Parse for FileName {
    fn parse(input: ParseStream) -> Result<Self> {
        let lit_file: syn::LitStr = input.parse()?;
        Ok(Self {
            filename: lit_file.value(),
        })
    }
}

#[proc_macro_derive(FromEnumOfTuples)]
pub fn my_derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let data: DataEnum = match input.data {
        Data::Enum(d) => d,
        _ => panic!("Only works on enum of unnamed tuples!!"),
    };
    let enum_ident = input.ident;
    let variants = data.variants.iter();
    let macro_out = variants.map(|v| {
        let var_id = &v.ident;
        if let Fields::Unnamed(fields) = v.fields.clone() {
            let unnamed_ident = fields.unnamed;
            let error_string = format!("{}::{}", enum_ident.to_string(), var_id.to_string());
            quote! {
                impl From<#enum_ident> for #unnamed_ident {
                    fn from(enum_ident: #enum_ident) -> Self {
                        match enum_ident {
                            #enum_ident::#var_id(item) => item,
                            _ => panic!("breaking conversion for {}", #error_string)
                        }
                    }
                }
                impl From<#unnamed_ident> for #enum_ident {
                    fn from(entry: #unnamed_ident) -> Self {
                        Self::#var_id(entry)
                    }
                }
            }
        } else {
            quote! {
                struct HelloWorld!;
            }
        }
    });
    let tokens = quote! {
        #(#macro_out)*
    };
    tokens.into()
}
