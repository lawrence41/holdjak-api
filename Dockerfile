FROM rust:1.31

WORKDIR /usr/src/holdjak-ui
COPY . .

RUN cargo install --path .

CMD ["holdjak-ui"]