#[macro_use]
extern crate rocket;
extern crate core;

pub mod admin;
pub mod credential;
#[cfg(feature = "firestore")]
pub mod document;
pub mod fairing;
#[cfg(feature = "firestore")]
pub mod firestore;
pub mod user;
#[cfg(feature = "firestore")]
pub mod value;
pub use firebase_admin_macro::*;
pub use firestore_grpc::google::firestore::v1::value::ValueType;
pub use firestore_grpc::google::firestore::v1::{ArrayValue, Document, MapValue, Value};
#[cfg(test)]
mod test {

    #[test]
    fn test() {}
}
