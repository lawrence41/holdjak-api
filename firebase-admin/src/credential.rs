#[allow(unused_variables, dead_code)]
use core::result::Result;
use core::result::Result::{Err, Ok};
use josekit::{
    jws::JwsHeader,
    jwt::{encode_with_signer, JwtPayload},
};
use reqwest::multipart::Form;
use serde::Deserialize;
use std::path::PathBuf;
use std::time::{SystemTime};

#[derive(Deserialize, Clone)]
pub struct ServiceAccountCredentials {
    #[serde(rename(deserialize = "type"))]
    pub(crate) cred_type: String,
    pub(crate) project_id: String,
    pub(crate) private_key_id: String,
    pub(crate) private_key: String,
    pub(crate) client_email: String,
    pub(crate) client_id: String,
    pub(crate) auth_uri: String,
    pub(crate) token_uri: String,
    pub(crate) auth_provider_x509_cert_url: String,
    pub(crate) client_x509_cert_url: String,
}
#[derive(Deserialize)]
struct AccessTokenResponseBody {
    access_token: String,
    expires_in: i32,
}
impl ServiceAccountCredentials {
    pub fn try_from_file<'a>(path: PathBuf) -> Result<Self, String>
    where
        Self: Deserialize<'a>,
    {
        Self::try_from_str(std::fs::read_to_string(path).unwrap().as_str())
    }

    pub fn try_from_str(contents: &str) -> Result<Self, String> {
        let maybe_json = serde_json::from_str::<Self>(contents);
        match maybe_json {
            Ok(res) => Ok(res),
            Err(err) => Err(err.to_string()),
        }
    }

    pub async fn create_access_token(&mut self) -> Result<(String, i32), reqwest::Error> {
        let access_token = self.create_access_token_request_jwt();
        let form = Form::new()
            .text("grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer")
            .text("assertion", access_token);
        let maybe_key = reqwest::Client::new()
            .post(&self.token_uri)
            .multipart(form)
            .send()
            .await
            .unwrap()
            .json::<AccessTokenResponseBody>()
            .await;
        match maybe_key {
            Ok(key) => Ok((key.access_token, key.expires_in)),
            Err(e) => Err(e),
        }
    }
    // based off https://developers.google.com/identity/protocols/oauth2/service-account "##Creating a JWT"
    fn create_access_token_request_jwt(&self) -> String {
        let signer = josekit::jws::RS256
            .signer_from_pem(self.private_key.as_str())
            .unwrap();

        let mut header_content: josekit::Map<String, josekit::Value> = josekit::Map::new();

        header_content.insert("alg".to_string(), josekit::Value::from("RS256"));
        header_content.insert("typ".to_string(), josekit::Value::from("JWT"));
        let mut payload_content: josekit::Map<String, josekit::Value> = josekit::Map::new();
        payload_content.insert(
            "iss".to_string(),
            josekit::Value::from(self.client_email.clone().as_str()),
        );
        // TODO: Make scopes a parameter
        payload_content.insert(
            "scope".to_string(),
            josekit::Value::from(
                "https://www.googleapis.com/auth/cloud-platform https://www.googleapis.com/auth/firebase.messaging"
            ),
        );
        payload_content.insert(
            "aud".to_string(),
            josekit::Value::from(self.token_uri.clone().as_str()),
        );
        let unix_now;
        unix_now = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
            Ok(n) => n.as_secs(),
            Err(_) => panic!("SystemTime before UNIX EPOCH!"),
        };
        // TODO: Consider making this just a max, but allow someone to make an access token shorter
        payload_content.insert("exp".to_string(), josekit::Value::from(unix_now + 3600));
        payload_content.insert("iat".to_string(), josekit::Value::from(unix_now));
        let header = JwsHeader::from_map(header_content.to_owned()).unwrap();
        let payload = JwtPayload::from_map(payload_content.to_owned()).unwrap();
        let key = encode_with_signer(&payload, &header, &signer);
        match key {
            Ok(inner) => inner,
            Err(e) => {
                println!("err: {:?}", e);
                "".to_owned()
            }
        }
    }
}
