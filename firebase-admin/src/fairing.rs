use futures::lock::Mutex;
use rocket::fairing::{Fairing, Info, Kind};
use rocket::{Build, Data, Request, Rocket};

use crate::admin::FirebaseAdmin;
use crate::credential::ServiceAccountCredentials;
#[cfg(feature = "firestore")]
use crate::firestore::Firestore;
use FirebaseConfigMode::Env;

pub struct FirebaseFairing {
    mode: FirebaseConfigMode,
    credentials: Option<ServiceAccountCredentials>,
}

#[async_trait]
impl Fairing for FirebaseFairing {
    fn info(&self) -> Info {
        Info {
            name: "Firebase Admin Fairing",
            kind: Kind::Ignite | Kind::Shutdown | Kind::Request | Kind::Singleton,
        }
    }

    async fn on_ignite(&self, rocket: Rocket<Build>) -> rocket::fairing::Result {
        let credential_ref = self.credentials.as_ref().unwrap();
        match &self.mode {
            Env => {
                #[cfg(feature = "firestore")]
                let rocket = rocket.manage(Mutex::new(Firestore::new(credential_ref)));
                Ok(rocket.manage(FirebaseAdmin {
                    credentials: self.credentials.clone(),
                    mode: Env,
                }))
            }
            _ => rocket::fairing::Result::Err(rocket.manage(FirebaseAdmin {
                credentials: None,
                mode: Env,
            })),
        }
    }

    async fn on_request(&self, _req: &mut Request<'_>, _data: &mut Data<'_>) {}
}

impl FirebaseFairing {
    pub fn new(mode: FirebaseConfigMode) -> Self {
        match mode {
            Env => {
                let credentials = env!("FIREBASE_CREDENTIALS");
                let maybe_credentials = ServiceAccountCredentials::try_from_str(credentials);
                if maybe_credentials.is_err() {
                    println!("Error Reading from {:?}", credentials);
                }
                Self {
                    mode,
                    credentials: maybe_credentials.ok().or(None),
                }
            }
            _ => panic!("Unsupported"),
        }
    }
}
pub enum FirebaseConfigMode {
    Env,
    CustomToken(Box<ServiceAccountCredentials>),
}
