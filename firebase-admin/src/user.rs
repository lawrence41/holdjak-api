use crate::admin::FirebaseAdmin;
use rocket::request::{FromRequest, Outcome};
use rocket::{Request, State};
use serde::{Deserialize, Serialize};
use std::fmt::Debug;

#[derive(Debug, Serialize, Deserialize)]
struct Claims {
    exp: String,
    iat: String,
    aud: String,
    iss: String,
    sub: String,
    auth_time: String,
}

pub struct FirebaseUser {
    pub user_id: String,
    pub authed: bool,
    pub name: String,
}

#[async_trait]
impl<'r> FromRequest<'r> for FirebaseUser {
    type Error = String;
    async fn from_request(request: &'r Request<'_>) -> Outcome<FirebaseUser, Self::Error> {
        let admin = request.guard::<&State<FirebaseAdmin>>().await.unwrap();
        match admin.verify_request(request).await {
            Ok(payload) => {
                let claim_set = payload.0.claims_set();
                let name = if let Some(name) = claim_set.get("name") {
                    name.as_str().unwrap()
                } else {
                    "anon"
                };
                let user_id = payload
                    .0
                    .claim("user_id")
                    .unwrap()
                    .as_str()
                    .unwrap()
                    .to_string();
                return Outcome::Success(FirebaseUser {
                    user_id,
                    authed: true,
                    name: name.to_string(),
                });
            }
            Err(e) => Outcome::Failure((rocket::http::Status::Unauthorized, e)),
        }
    }
}
