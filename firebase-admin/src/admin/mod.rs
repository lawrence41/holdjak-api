use std::collections::HashMap;
use std::thread::sleep;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use josekit::jws::{JwsHeader, RS256};
use josekit::jwt::{decode_with_verifier, JwtPayload};
use jsonwebtoken::{decode_header, Algorithm};
use openssl::x509::X509;
use rocket::form::validate::Contains;
use rocket::serde::json::serde_json;
use rocket::Request;

use crate::credential::ServiceAccountCredentials;
use crate::fairing::FirebaseConfigMode;

//TODO: Figure out how to have this not as a string constant. It really should come from some secret
pub const CERTS_URL: &'_ str =
    "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com";

pub struct FirebaseAdmin {
    pub(crate) mode: FirebaseConfigMode,
    pub(crate) credentials: Option<ServiceAccountCredentials>,
}

impl FirebaseAdmin {
    pub async fn verify_request(
        &self,
        request: &Request<'_>,
    ) -> Result<(JwtPayload, JwsHeader), String> {
        let maybe_auth_header = request.headers().get_one("authorization");
        if let Some(auth_header) = maybe_auth_header {
            let id_token = &auth_header[7..];
            let certificates: HashMap<String, String> =
                Self::get_x509_certificates_from_url(CERTS_URL).await;
            match self.verify_id_token(id_token, certificates) {
                Ok(res) => {
                    return Ok(res);
                }
                Err(err) => Err(err.to_string()),
            }
        } else {
            return Err("No Header".to_string());
        }
    }

    pub fn verify_id_token(
        &self,
        id_token: &str,
        certs: HashMap<String, String>,
    ) -> Result<(JwtPayload, JwsHeader), String> {
        if self.credentials.is_none() {
            return Err("No Credentials!".to_string());
        }
        info!("🔥🤨 Verifying Firebase Token...");
        if let Ok(header) = decode_header(id_token) {
            info!("    >> 🤯 Decoded Header");
            let alg = header.alg;
            if alg != Algorithm::RS256 {
                return Err("Wrong Algorithm!".to_string());
            }
            info!("    >> 🧩 Using RS256!");
            //	Must correspond to one of the public keys listed at https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com
            let kid = header.kid.unwrap();
            if !certs.contains_key(kid.as_str()) {
                return Err("No Matching Cert for kid!".to_string());
            }
            info!("    >> 🏛️ {kid:?} kid has matching cert");
            let cert = certs.get(kid.as_str()).unwrap();
            if let Ok(xcert) = X509::from_pem(cert.as_str().as_bytes()) {
                let public_key = xcert.public_key().unwrap();
                let public_der = public_key.public_key_to_der().unwrap();
                let verifier = RS256.verifier_from_der(public_der).unwrap();
                if let Ok(res) = decode_with_verifier(id_token, &verifier) {
                    info!("    >> ✅ token was signed with cert");
                    let (payload, _) = &res;
                    let now = SystemTime::now();
                    let maybe_freshness = payload.expires_at().unwrap().duration_since(now);
                    // exp 	Must be in the future. The time is measured in seconds since the UNIX epoch.
                    if let Err(err) = maybe_freshness {
                        return Err(format!("{:?}", err));
                    }
                    info!(
                        "    >> 🥩 Valid expiration date! Token has {:?} minutes left!",
                        maybe_freshness.unwrap().as_secs() / 60
                    );
                    // iat must be in the past
                    let secs ;
                    let maybe_valid_issue = now.duration_since(payload.issued_at().unwrap());
                    if let Err(err) = maybe_valid_issue {
                        secs = err.duration().as_secs();
                        if secs < 5 {
                            println!("In the futures?");
                            sleep(Duration::from_secs(secs));
                        } else {
                            return Err(format!("{:?}", err));
                        }
                    } else {
                        secs = maybe_valid_issue.unwrap().as_secs()
                    }
                    info!(
                        "    >> 📰 Valid Issue date! Token was issued {:?} sec ago!",
                        secs
                    );
                    let credentials = self.credentials.as_ref().unwrap();
                    // aud must match project id
                    let aud = payload.audience().unwrap();
                    if !aud.contains(credentials.project_id.as_str()) {
                        return Err("Not in audience!".to_string());
                    }
                    info!("    >> 👥 Valid project id in aud! {aud:?}",);
                    // iss must be in the correct format below
                    let issuer = payload.issuer().unwrap();
                    let issuer_to_match =
                        format!("https://securetoken.google.com/{}", credentials.project_id);
                    if !issuer.eq(&issuer_to_match) {
                        return Err("Incorrect Issuer!".to_string());
                    }
                    info!("    >>👩‍⚖Valid Issuer URL! {issuer_to_match:?}",);
                    // sub must not be empty _this is the device id_
                    if payload.subject().unwrap().len() == 0 {
                        return Err("No subject!".to_string());
                    }
                    info!("    >>🧑‍💻Subject Exists!",);
                    // auth time must be in the past
                    let auth_time = payload.claim("auth_time").unwrap().as_u64().unwrap();
                    if auth_time > now.duration_since(UNIX_EPOCH).unwrap().as_secs() {
                        // return Err("Authenticated in the future!?".to_string());
                    }
                    info!("    >> ⌚ Valid auth_time!",);
                    info!("🎈 Token Validated! 🎈");
                    return Ok(res);
                }
            }

            return Err("Invalid KID".to_string());
        }
        Err("Invalid Header".to_string())
    }
    async fn get_x509_certificates_from_url(cert_url: &str) -> HashMap<String, String> {
        let res = reqwest::get(cert_url).await.unwrap().bytes().await.unwrap();
        serde_json::from_slice(res.to_vec().as_slice()).unwrap()
    }
}
