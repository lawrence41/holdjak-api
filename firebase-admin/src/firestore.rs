use std::collections::HashMap;
use std::ops::Add;
use std::time::{Duration, Instant};

use crate::credential::ServiceAccountCredentials;
use crate::value::FirestoreValue;
use firestore_grpc::google::firestore::v1::{
    GetDocumentRequest, Value,
};
use firestore_grpc::tonic::Code;
use firestore_grpc::tonic::{
    codegen::InterceptedService,
    metadata::MetadataValue,
    transport::{Channel, ClientTlsConfig},
    Request, Status,
};
use firestore_grpc::v1::{
    firestore_client::FirestoreClient, CreateDocumentRequest, Document, UpdateDocumentRequest,
};
use reqwest::Error;
use rocket::form::validate::Contains;

pub type BoxError = Box<dyn std::error::Error + Sync + Send + 'static>;

const URL: &str = "https://firestore.googleapis.com";
const DOMAIN: &str = "firestore.googleapis.com";

pub struct Firestore {
    pub(crate) credentials: ServiceAccountCredentials,
    admin_key: Option<String>,
    token_expires_in: Instant,
}

impl Firestore {
    pub fn new(credentials: &ServiceAccountCredentials) -> Self {
        Firestore {
            credentials: credentials.clone(),
            admin_key: None,
            token_expires_in: Instant::now(),
        }
    }

    async fn get_fresh_access_token(&mut self) -> Result<String, reqwest::Error> {
        let maybe_key = self.credentials.create_access_token().await;
        match maybe_key {
            Ok((key, expires_in)) => {
                self.admin_key = Some(key.clone());
                self.token_expires_in = Instant::now().add(Duration::from_secs(expires_in as u64));
                Ok(key)
            }
            Err(e) => Err(e),
        }
    }
    pub async fn get_firestore_document<T>(&mut self, name: &str) -> Result<T, Error>
    where
        T: From<FirestoreValue>,
    {
        let res = self
            .get_document(format!("{}/{}", self.get_parent(), name))
            .await;
        match res {
            Ok(document) => {
                let firestore_value = FirestoreValue::from(document.fields);
                Ok(firestore_value.into())
            }
            Err(e) => Err(e),
        }
    }
    async fn get_document(&mut self, name: String) -> Result<Document, Error> {
        let get_document_request = GetDocumentRequest {
            name,
            mask: None,
            consistency_selector: None,
        };
        let key = self.get_admin_key().await.unwrap();
        let mut client = Self::get_client(key).await.unwrap();
        let document = client.get_document(get_document_request).await.unwrap();
        Ok(document.into_inner())
    }

    // TODO: Convert this to Result
    async fn get_admin_key(&mut self) -> Result<String, Error> {
        match &self.admin_key {
            Some(admin_key) => {
                if !self.token_expires_in.gt(&Instant::now()) {
                    return self.get_fresh_access_token().await;
                }
                Ok(admin_key.clone())
            }
            None => self.get_fresh_access_token().await,
        }
    }
    fn get_parent(&self) -> String {
        format!(
            "projects/{}/databases/(default)/documents",
            self.credentials.project_id.clone()
        )
    }
    pub async fn upsert_document(
        &mut self,
        name: &str,
        value: FirestoreValue,
    ) -> Result<(), FirebaseError> {
        let name_split = name.clone().split("/").collect::<Vec<&str>>();
        let document_path = name_split.last().unwrap();
        name_split.join("/");
        let db_path = self.get_parent().add("/").add(name);

        let maybe_fields: Result<HashMap<String, Value>, ()> = value.try_into();
        if maybe_fields.is_err() {
            return Err(FirebaseError::InvalidValue(
                "Can only use FirestoreValues with Maps!".to_string(),
            ));
        }
        let res = self
            .update_document(name, maybe_fields.clone().unwrap())
            .await;
        if res.is_err() {
            println!("Document not found... Trying to create");
            let collection_id = name_split.first().unwrap();
            let res = self
                .create_document(
                    collection_id.to_string(),
                    Some(document_path.to_string()),
                    maybe_fields.unwrap(),
                )
                .await;
            if let Err(e) = res {
                println!("{e:?}");
                return Err(FirebaseError::UnknownIssue);
            }
        }
        println!("{db_path}");
        Ok(())
    }
    async fn create_document(
        &mut self,
        collection_id: String,
        document_id: Option<String>,
        document_contents: HashMap<String, Value>,
    ) -> Result<(), FirebaseError> {
        let maybe_key = self.get_admin_key().await;
        match maybe_key {
            Ok(key) => {
                let mut client = Self::get_client(key).await.unwrap();
                let req = CreateDocumentRequest {
                    parent: self.get_parent(),
                    collection_id,
                    document_id: document_id.unwrap_or("".to_string()),
                    document: Some(Document {
                        name: "".to_string(),
                        fields: document_contents,
                        create_time: None,
                        update_time: None,
                    }),
                    mask: None,
                };
                let res = client.create_document(req).await;
                println!("{res:?}");
                Ok(())
            }
            Err(e) => Err(FirebaseError::TokenError(format!("Token Error! {}", e))),
        }
    }
    pub async fn update_document(
        &mut self,
        name_suffix: &str,
        document_contents: HashMap<String, Value>,
    ) -> Result<(), FirebaseError> {
        let name = format!("{}/{}", self.get_parent(), name_suffix);
        let maybe_key = self.get_admin_key().await;
        if name.contains('-') {
            return Err(FirebaseError::InvalidArgument(
                "Found a hyphen which is forbidden!".to_string(),
            ));
        }
        match maybe_key {
            Ok(key) => {
                let mut client = Self::get_client(key).await.unwrap();
                let req = UpdateDocumentRequest {
                    document: Some(Document {
                        name,
                        fields: document_contents,
                        create_time: None,
                        update_time: None,
                    }),
                    mask: None,
                    update_mask: None,
                    current_document: None,
                };
                let res = client.update_document(req).await;
                match res {
                    Ok(_) => Ok(()),
                    Err(status) => match status.code() {
                        Code::NotFound => Err(FirebaseError::DocumentNotFound),
                        Code::InvalidArgument => Err(FirebaseError::ConfigurationError),
                        _ => Err(FirebaseError::UnknownIssue),
                    },
                }
            }
            Err(_) => Err(FirebaseError::TokenError("No Token".to_string())),
        }
    }

    pub async fn get_client(
        key: String,
    ) -> Result<
        FirestoreClient<
            InterceptedService<Channel, impl Fn(Request<()>) -> Result<Request<()>, Status>>,
        >,
        BoxError,
    > {
        let endpoint =
            Channel::from_static(URL).tls_config(ClientTlsConfig::new().domain_name(DOMAIN));
        let bearer_token = format!("Bearer {}", key);
        let header_value = MetadataValue::from_str(&bearer_token)?;
        let channel = endpoint?.connect().await?;
        let service = FirestoreClient::with_interceptor(channel, move |mut req: Request<()>| {
            req.metadata_mut()
                .insert("authorization", header_value.clone());
            Ok(req)
        });
        Ok(service)
    }
}

#[derive(Debug)]
pub enum FirebaseError {
    TokenError(String),
    FirestoreError(String),
    InvalidPathError(String),
    InvalidValue(String),
    InvalidArgument(String),
    ConfigurationError,
    DocumentNotFound,
    UnknownIssue,
}
